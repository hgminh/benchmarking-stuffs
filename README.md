# benchmarking-stuffs (WIP)

Containing a lot of micro benchmarks.

## Requirement

- C++ compiler with C++ 17 support
- bazel
- Linux

## How to use

To build, run the following commands

```
$ bazel build -c opt <target>

e.g.

$ bazel build -c opt bm_string
$ bazel build -c opt bm_memory_access
```

After that, binary files will be generated inside `bazel-bin` folder. To run
the benchmark, just run these executables.

```
$ bazel-bin/bm_string
```

Note: You may need to disable CPU scaling to get more accurate results

```
# before running benchmark
$ cpupower frequency-set --governor performance
# after running benhmark
$ cpupower frequency-set --governor powersave
```
