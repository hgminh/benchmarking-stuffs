#include <benchmark/benchmark.h>
#include <array>

constexpr int MAX_N = 1000;

static void BM_SequentialAccess(benchmark::State &state) {
  std::array<std::array<int, MAX_N>, MAX_N> arr2d;
  for (auto _ : state) {
    volatile int x;
    for (int i = 0; i < MAX_N; ++i)
      for (int j = 0; j < MAX_N; ++j)
        x = arr2d[i][j];
    (void)x;
  }
}

BENCHMARK(BM_SequentialAccess);

static void BM_NonSequentialAccess(benchmark::State &state) {
  std::array<std::array<int, MAX_N>, MAX_N> arr2d;
  for (auto _ : state) {
    volatile int x;
    for (int i = 0; i < MAX_N; ++i)
      for (int j = 0; j < MAX_N; ++j)
        x = arr2d[j][i];
    (void)x;
  }
}

BENCHMARK(BM_NonSequentialAccess);

BENCHMARK_MAIN();
