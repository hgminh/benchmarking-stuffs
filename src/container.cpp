#include <benchmark/benchmark.h>

#include <list>
#include <deque>

struct CustomLinkedList {
  struct Node {
    char x;
    Node *next;
  };

  Node *arr;
  int len;

  CustomLinkedList(int len, char val) {
    this->len = len;
    arr = new Node[len];

    for (int i = 0; i < len; ++i) {
      auto &node = arr[i];
      node.x = val;

      if (i == len - 1)
        node.next = nullptr;
      else
        node.next = &arr[i + 1];
    }
  }

  ~CustomLinkedList() {
    delete [] arr;
  }

};

static void BM_CustomLinkedListTraversal(benchmark::State &state) {
  CustomLinkedList list(state.range(0), 'X');

  for (auto _ : state) {
    volatile char temp;
    auto node = &list.arr[0];
    while (node != nullptr) {
      temp = node->x;
      node = node->next;
    }
    (void)temp;
  }
}

static void BM_CustomLinkedListTraversalWithNoPointer(benchmark::State &state) {
  CustomLinkedList list(state.range(0), 'X');

  for (auto _ : state) {
    volatile char temp;
    for (int i = 0; i < list.len; ++i) {
      temp = list.arr[i].x;
    }
    (void)temp;
  }
}

static void BM_CustomLinkedListTraversalRandomAccess(benchmark::State &state) {
  CustomLinkedList list(state.range(0), 'X');

  for (auto _ : state) {
    volatile char temp;
    for (int i = 0; i < list.len; ++i) {
      int r = i * 1997 % list.len; // fake random, because rand() costs too much
      temp = list.arr[r].x;
    }
    (void)temp;
  }
}

static void BM_VectorPushBack(benchmark::State &state) {
  for (auto _ : state) {
    std::vector<char> vec;
    for (int i = 0; i < state.range(0); ++i)
      vec.push_back('X');
  }
}

static void BM_VectorTraversal(benchmark::State &state) {
  std::vector<char> vec(state.range(0), 'X');
  for (auto _ : state) {
    volatile char temp;
    for (auto ch : vec) {
      temp = ch;
    }
    (void)temp;
  }
}

static void BM_StringPushBack(benchmark::State &state) {
  for (auto _ : state) {
    std::string str;
    for (int i = 0; i < state.range(0); ++i)
      str += 'X';
  }
}

static void BM_StringTraversal(benchmark::State &state) {
  std::string str(state.range(0), 'X');
  for (auto _ : state) {
    volatile char temp;
    for (auto ch : str) {
      temp = ch;
    }
    (void)temp;
  }
}

static void BM_ListPushBack(benchmark::State &state) {
  for (auto _ : state) {
    std::list<char> linked_list;
    for (int i = 0; i < state.range(0); ++i)
      linked_list.push_back('X');
  }
}

static void BM_ListTraversal(benchmark::State &state) {
  std::list<char> linked_list(state.range(0), 'X');
  for (auto _ : state) {
    for (auto ch : linked_list) {
      (void)ch;
    }
  }
}

static void BM_DequePushBack(benchmark::State &state) {
  for (auto _ : state) {
    std::deque<char> queue;
    for (int i = 0; i < state.range(0); ++i)
      queue.push_back('X');
  }
}

static void BM_DequeTraversal(benchmark::State &state) {
  std::deque<char> queue(state.range(0), 'X');
  for (auto _ : state) {
    for (auto ch : queue) {
      (void)ch;
    }
  }
}

BENCHMARK(BM_VectorPushBack)->RangeMultiplier(4)->Range(16, 16 << 10);
BENCHMARK(BM_StringPushBack)->RangeMultiplier(4)->Range(16, 16 << 10);
BENCHMARK(BM_ListPushBack)->RangeMultiplier(4)->Range(16, 16 << 10);
BENCHMARK(BM_DequePushBack)->RangeMultiplier(4)->Range(16, 16 << 10);

BENCHMARK(BM_VectorTraversal)->RangeMultiplier(8)->Range(16, 16 << 12);
BENCHMARK(BM_StringTraversal)->RangeMultiplier(8)->Range(16, 16 << 12);
BENCHMARK(BM_ListTraversal)->RangeMultiplier(8)->Range(16, 16 << 12);
BENCHMARK(BM_DequeTraversal)->RangeMultiplier(8)->Range(16, 16 << 12);
BENCHMARK(BM_CustomLinkedListTraversal)->RangeMultiplier(8)->Range(16, 16 << 12);
BENCHMARK(BM_CustomLinkedListTraversalWithNoPointer)->RangeMultiplier(8)->Range(16, 16 << 12);
BENCHMARK(BM_CustomLinkedListTraversalRandomAccess)->RangeMultiplier(8)->Range(16, 16 << 12);

BENCHMARK_MAIN();
