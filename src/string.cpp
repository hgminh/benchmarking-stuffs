#include <benchmark/benchmark.h>

static void BM_StringCreation(benchmark::State &state) {
  for (auto _ : state)
    std::string empty_string(state.range(0), 'C');
}

BENCHMARK(BM_StringCreation)->RangeMultiplier(2)->Range(2, 2 << 10);

static void BM_StringCopy(benchmark::State &state) {
  std::string x(state.range(0), 'C');
  for (auto _ : state)
    std::string copy(x);
}

BENCHMARK(BM_StringCopy)->RangeMultiplier(2)->Range(2, 2 << 10);

BENCHMARK_MAIN();
