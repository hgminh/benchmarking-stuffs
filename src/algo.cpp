#include <benchmark/benchmark.h>

#include <algorithm>
#include <cstring>

static void BM_StdFill(benchmark::State &state) {
  for (auto _ : state) {
    std::vector<int> vec(state.range(0), 5);
    std::fill(vec.begin(), vec.end(), 0);
  }
}

static void BM_StdCopy(benchmark::State &state) {
  for (auto _ : state) {
    std::vector<int> vec1(state.range(0), 5);
    std::vector<int> vec2(state.range(0), 4);
    std::copy(begin(vec1), end(vec1), begin(vec1));
  }
}

static void BM_MemSet(benchmark::State &state) {
  for (auto _ : state) {
    std::vector<int> vec(state.range(0), 5);
    memset((void*) vec.data(), 0, vec.size() * sizeof(int));
  }
}

static void BM_MemCpy(benchmark::State &state) {
  for (auto _ : state) {
    std::vector<int> vec1(state.range(0), 5);
    std::vector<int> vec2(state.range(0), 4);
    memcpy(vec1.data(), vec2.data(), vec1.size() * sizeof(int));
  }
}

BENCHMARK(BM_StdFill)->RangeMultiplier(8)->Range(16, 16 << 12);
BENCHMARK(BM_MemSet)->RangeMultiplier(8)->Range(16, 16 << 12);

BENCHMARK(BM_StdCopy)->RangeMultiplier(8)->Range(16, 16 << 12);
BENCHMARK(BM_MemCpy)->RangeMultiplier(9)->Range(16, 16 << 12);

BENCHMARK_MAIN();
